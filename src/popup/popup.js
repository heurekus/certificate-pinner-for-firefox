"use strict";

/**
* Certificate Pinner for Firefox
*
* Pop-up routines
*
* When the user presses the small icon of this web-extension in the browsers
* status bar, a small window will open in Firefox with buttons to add the
* certificate of the current tab to the local store, i.e. to pin it. 
* 
* Firefox mobile opens a new tab instead of a pop-up.
* 
* For debugging, a button is available to invalidate all certificates
* pinned. This is not written to local store so the originally stored 
* fingerprints are restored when the browser is restarted.
* 
* @version    1.0 2019-06-19
* @package    cert-pinner
* @copyright  Copyright (c) 2019 Martin Sauter
* @license    GNU General Public License
* @since      Since Release 1.0
* 
*/

// Get the background page to access a number of variables and objects
const bgPage = browser.extension.getBackgroundPage();

bgPage.certDebugLog('Pinned Certs');
bgPage.certDebugLog('Length: ' + bgPage.pinnedCerts.length)
bgPage.certDebugLog(bgPage.pinnedCerts);
  
bgPage.certDebugLog('Saved TAB Certs:')
bgPage.certDebugLog('Length: ' + bgPage.domainsAndCertsOfTabs.length)
bgPage.certDebugLog(bgPage.domainsAndCertsOfTabs); 

// Depending on whether the domain of the tab is pinned or not, show
// or hide pin/un-pin buttons.
// 
// TODO: Only works on the desktop for the moment as android can't handle 
// document.queryselector for pop-ups that are tabs on the mobile platform.
if (bgPage.operatingSystem != "android") showOrHidePinButtons();

document.getElementById("newPinBtn").addEventListener("click", pinThisCert);
document.getElementById("newPinPubKeyBtn").addEventListener("click", pinThisCert);
document.getElementById("unPinBtn").addEventListener("click", unpinThisCert);
document.getElementById("showPinBtn").addEventListener("click", showAllCerts);


/*****************************************************************************
 * 
 * Event handlers
 * 
 */

function pinThisCert() {
	
	getCurrentWindowTabs().then((tabs) => {

		var domain = getDomainOfActiveTab(tabs);
		var domainInfo = bgPage.getInfoForDomain(domain);
		
		var fingerprint = domainInfo.fingerprint;
		var subjPubKeyDigest = domainInfo.subjPubKeyDigest;
		var validity = domainInfo.validity;
		
		bgPage.certDebugLog('cert-pin: domain-info:');
		bgPage.certDebugLog(domainInfo);
		
		// In rare cases background.js does not get called for TLS 
		// protected pages. The only example found so far is
		// https://accounts.firefox.com. In this case, no 
		// domain info or fingerprint is available and the domain
		// therefore can't be pinned.
		if (fingerprint === false || domainInfo === false) {
			alert('ERROR: Fingerprint for certificate not found');
			return;
		}
		
		var result = false;
		
		// Pin the certificate either with the fingerprint or with the
		// digest has of the public key
		if (this.id == "newPinBtn") {
			// Pin the fingerprint of the certificate
			result = bgPage.replaceOrRemoveAllPins(domain, fingerprint, "", 
                     validity);
			
			bgPage.certDebugLog('cert-pin: Fingerprint used for pinning. ' + 
					            'Result: ' + result);
			
		} else {
			// Pin the pub key has of the certificate
			result = bgPage.replaceOrRemoveAllPins(domain, "", subjPubKeyDigest,
                     validity);
			bgPage.certDebugLog('cert-pin: Pub. Key used for pinning. ' + 
					            'Result: ' + result);
		}
			
		if (result) {
			bgPage.showPinOkLogo()
			
			if (bgPage.operatingSystem == "android") {
				alert('Page pinned, please close this tab and reload!');
			}

		} else {
			alert('ERROR: Unable to add fingerprint!?');
		}		
		
		window.close();
		
	});	
}


function unpinThisCert() {
		
	getCurrentWindowTabs().then((tabs) => {

		var domain = getDomainOfActiveTab(tabs);
		
		if (bgPage.replaceOrRemoveAllPins(domain, false, "", false)) {
			
			bgPage.removePinOkLogo();
			
			if (bgPage.operatingSystem == "android") {
				alert('Page un-pinned, you can now close this tab!');
			}
			
		} else {

			alert('ERROR: Unable to remove fingerprint!?');			
		}
		
		window.close();

	});	
}


function showAllCerts(){

	// Open a new tab
	var localURL = browser.extension.getURL("show_all/show_all.html");
	
	let createData = {
		url: localURL
	};
	
	let creating = browser.tabs.create(createData);
	creating.then(() => {
		  bgPage.certDebugLog("cert-pin: Show all TAB created!");
	});
	
	window.close();
	
}


/*****************************************************************************
 * 
 * Support functions
 * 
 */


function getCurrentWindowTabs() {
	return browser.tabs.query({currentWindow: true});
}


function getDomainOfActiveTab(tabs) {
	var domain = '';
	for (let tab of tabs) {
		if (tab.active) {
			bgPage.certDebugLog('cert-pin: Active tab is: ' + tab.id);
			bgPage.certDebugLog('cert-pin: Domain is: ' + bgPage.getDomain(tab.url))
						
			domain = bgPage.getDomain(tab.url);
		}
	}
	return(domain);
}


function getProtocolUsedInActiveTab(tabs) {
	for (let tab of tabs) {
		if (tab.active) {
			bgPage.certDebugLog('cert-pin: Protocol used in tab: ' + 
					             bgPage.getProtocolPartOfURL(tab.url));
			return bgPage.getProtocolPartOfURL(tab.url);
		}
	}
}

function getRemainingValidityDaysString(domain) {
	
	bgPage.certDebugLog('Getting number of remaining validity dates');
	
	let domainInfo = bgPage.getPinnedDomainInfo(domain);
	bgPage.certDebugLog(domainInfo);
	
	// If there is more than one certificate pinned for the domain
	// it is not possible to calculate the number of days the certificate
	// is valid. Return a string.
	if (domainInfo.length > 1) {
		return("Validity: Multiple certs, can't say");
	}
	
	// If no info was returned there is a problem
	if (domainInfo.length != 1) {
		return("Validity: Not applicable");
	}

	let now = Date.now();
		let validityEnd = domainInfo[0].validity;
	// Note: Dates are given in milliseconds since 1970
	let remainingDays = Math.trunc((validityEnd - now) / 86400 / 1000);	
	bgPage.certDebugLog("Pinned cert valid for: " + remainingDays + " days");
	
    let returnString = "Valid for: " + remainingDays + " day";
    if (remainingDays > 1) {
    	returnString += "s";
    }
	
	return returnString;
}


/**
 * 
 * showOrHidePinButtons()
 * 
 * Hides or shows the 'Pin' and 'Un-Pin' buttons depending on whether a
 * pinned or not-pinned domain name is loaded in the current tab.
 * 
 * Note: The function is declared as 'async' as it needs to wait
 * itself for the tabs info which is returned asynchronously and must
 * thus not lead to overall blocking of the event loop. In other words
 * it returns straight away and the buttons are shown or hidden as soon
 * as the tabs info object is available. 
 * 
 * @param none
 * 
 * @returns none
 * 
 */

async function showOrHidePinButtons() {

	let tabs = await getCurrentWindowTabs();

	// Hide both pin and un-pin buttons if main page of tab wasn't loaded
	// over https.
	if (getProtocolUsedInActiveTab(tabs) != 'https') {
		document.querySelector(".unPinBtnInClass").classList.add("hidden");
		document.querySelector(".newPinBtnInClass").classList.add("hidden");
		document.querySelector(".newPubKeyPinBtnInClass").classList.add("hidden");
		
		return;
	}
	
	// Page was loaded with https, let's see if it is pinned
	
	var domain = getDomainOfActiveTab(tabs);
	var domainInfo = bgPage.getInfoForDomain(domain);
	
	bgPage.certDebugLog("cert-pin showOrHidePinButtons()");

	// If the page is pinned
	if (bgPage.isDomainPinned(domain)) {
		
		// Hide the pinning buttons as page is already pinned 
		document.querySelector(".newPinBtnInClass").classList.add("hidden");
		document.querySelector(".newPubKeyPinBtnInClass").classList.add("hidden");
		
		// Show how many days the certificate is still valid
		let daysStr = getRemainingValidityDaysString(domain);		
		document.getElementById("validityDays").innerHTML = "&nbsp;" + daysStr;
		
	}
	else {
		// The page is NOT pinned
		
		// Hide the 'Un-Pin' button
		document.querySelector(".unPinBtnInClass").classList.add("hidden");		

		// The page can be pinned. Check if pinning on Pub Key option is 
		// enabled. If not, hide the pin on pub key button.
		if (bgPage.pubKeyPinningOn === false) {
			document.querySelector(".newPubKeyPinBtnInClass").classList.add("hidden");		
		}
	}
}
