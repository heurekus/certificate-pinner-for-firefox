"use strict";

/**
* Certificate Pinner for Firefox
*
* Options routines
* 
* In about:addons, a number of debug options such as debug msgs on/off and
* invalidate all certificates for testing are shown. This JS file handles
* the user input for it.
* 
* 
* @version    1.0 2019-06-22
* @package    cert-pinner
* @copyright  Copyright (c) 2019 Martin Sauter
* @license    GNU General Public License
* @since      Since Release 1.0
* 
*/

// Get the background page to access a number of variables and objects
// and set the checkboxes based on their content.
const bgPage = browser.extension.getBackgroundPage();

if(bgPage.outputDebugMessages == true) {
	document.getElementById("debugOnOff").checked = true;
}

if(bgPage.pubKeyPinningOn == true) {
	document.getElementById("pubKeyPinningOn").checked = true;
}

if(bgPage.localDateTimeFormat == "de-DE") {
	document.getElementById("forceGermanDateFormat").checked = true
}


document.getElementById("debugOnOff").addEventListener("click", handleDebugOnOff);
document.getElementById("pubKeyPinningOn").addEventListener("click", handlePubPin);
document.getElementById("forceGermanDateFormat").addEventListener("click", handleForceDateFormat);

function handleDebugOnOff() {
	  var checkBox = document.getElementById("debugOnOff");

	  if (checkBox.checked == true){
		  bgPage.outputDebugMessages = true;		  
		  console.log('cert-pin: debug messages activated');
	  } else {
		  bgPage.outputDebugMessages = false;
		  console.log('cert-pin: debug messages DE-activated');
	  }
	  
	  bgPage.storeDebugMsgOutputStatus(bgPage.outputDebugMessages);
}

function handlePubPin() {
	  var checkBox = document.getElementById("pubKeyPinningOn");

	  if (checkBox.checked == true){
		  bgPage.pubKeyPinningOn = true;		  
		  console.log('cert-pin: Pub Key Pinning: ON');
	  } else {
		  bgPage.pubKeyPinningOn = false;
		  console.log('cert-pin: Pub Key Pinning: OFF');
	  }
	  
	  bgPage.storePubKeyPinningOnStatus(bgPage.pubKeyPinningOn);
	
}

function handleForceDateFormat() {
	var checkBox = document.getElementById("forceGermanDateFormat");

	  if (checkBox.checked == true){
		  bgPage.localDateTimeFormat = "de-DE";		  
		  console.log('cert-pin: Force German date format: ON');
	  } else {
		  bgPage.localDateTimeFormat = window.navigator.language;
		  console.log('cert-pin: Force German date format: OFF');
	  }

	  bgPage.storeDateFormat(bgPage.localDateTimeFormat);
	  
}



