# Certificate Pinner For Firefox

### Overview

This Web-Extension add-on for Firefox (FF) allows to 'pin' TLS certificates, i.e. to store their fingerprints in FF' local storage. Whenever a page is loaded and the connection is TLS encrypted, it compares the fingerprint of the presented TLS certificate to the one that is stored. If they don't match, the TLS authentication process is interrupted before any local secrets such as session cookies, passwords, etc. are sent to the server. A new tab is opened to show the user the new certificate which might be genuine or not. If accepted, the user can replace the old fingerprint with the new fingerprint in local storage. A new button in the browser's toolbar opens a pop-up menu to pin and un-pin page certificates and to get a list of all pinned certificates.



### Install the Packaged Add-on

The latest version is available in the Firefox Add-on store: https://addons.mozilla.org/en-US/firefox/addon/certificate-pinner/

### Temporary Development Installation from Source

For test purposes, open a tab in FF and type 'about:debugging'. Then temporarily load the add-on from there by selecting the manifest file in the 'src' directory. For details see https://developer.mozilla.org/de/docs/Tools/about:debugging.


### How to Use The Add-on

![](images/firefox-new-icon-for-cert-pin.png)

Once installed, a little icon becomes available in the browser's toolbar. Once a page has been loaded in a tab, click on the icon and select "Pin This Certificate". Next time the page is loaded, a little green 'P' (=pinned) logo will be added to the icon to confirm that the domain is pinned and the pinning check was successful.


### Test pinning

Use the 'Test Invalidate' button to overwrite the saved certificate fingerprints with a default value. This will create certificate fingerprint mismatches in the same way as if a different certificate was sent, triggering a page load abort and a new tab with information that the certificate fingerprint does not match the stored fingerprint. In this 'alert' tab, a button is available to update the stored fingerprint.  

### Optional Public Key Pinning

By default, the public certificate is pinned based on the fingerprint that is also shown when inspecting the certificate details in Firefox. This means that a certificate warning is shown for all new certificates even if the public key is re-used. For those who would not like to get a warning in this scenario, it is also possible to pin on the public key. This option is disabled by default and has to be activated in the add-on's preference section.

### Notification For Expiring Certificates
A notification will be shown when a pinned certificate is about to expire within 10 days via the OS's notification system. If you pin your own certificates then this might just be *the* reminder that prevents that 'oops' down the road. For sanity reasons, only two notifications will be shown for each certificate that is about to expire until the browser is restarted. 
Note: Android doesn't support notifications from Firefox so don't expect to see anything there. 

### Firefox on Android

The add-on will work on Android but the presentation of information is not yet optimized for it.


 


 
 