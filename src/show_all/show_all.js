"use strict";

/**
* Certificate Pinner for Firefox
*
* Show all pinned certificates
*
* 
* @version    1.0 2019-06-20
* @package    cert-pinner
* @copyright  Copyright (c) 2019 Martin Sauter
* @license    GNU General Public License
* @since      Since Release 1.0
* 
*/

// Get the background page to access the offending certificate info
const bgPage = browser.extension.getBackgroundPage();

checkIfDomainIsInUrlAndRemoveFromList();
checkIfFingerprintIsInUrlAndInvalidate();

var pageUrl = getBaseUrlOfTab();

createHtmlTableSubjectEntry('<strong>Domain /<br>Fingerprint (sha256)</strong>', 
		                    '<strong>Pinned /<br> Valid Until</strong>', false);

bgPage.pinnedCerts.forEach(pin => {

	bgPage.certDebugLog(pin);
	var p = new Date(pin.pinned);
	var p_str = p.toLocaleDateString(bgPage.localDateTimeFormat) + ', ' + 
	            p.toLocaleTimeString(bgPage.localDateTimeFormat);

	var v = new Date(pin.validity);
	var v_str = v.toLocaleDateString(bgPage.localDateTimeFormat) + ', ' + 
	            v.toLocaleTimeString(bgPage.localDateTimeFormat);
	
	createHtmlTableSubjectEntry('<strong>' + pin.subject + '</strong>', 
			                    p_str + '<br>' + v_str, 
			                    pageUrl + '?domain=' + pin.subject);

	createHtmlTableCertEntry(pin);
});


/*****************************************************************************
 * 
 * Event Handlers and support functions
 * 
 */

function createHtmlTableCertEntry(pin) {
	
	let invalidateUrl = pageUrl + '?dm=' + pin.subject + '&fp=' + pin.fingerprint;
	
	let entryTable = document.querySelector(".show-all-table");
	
	let entryTR = document.createElement("tr");
	let entryName =  document.createElement("td");
	let entryValue =  document.createElement("td");

	let link =  document.createElement("a");
	link.setAttribute("href", invalidateUrl);
	let linkText = document.createTextNode("Invalidate for testing");
	link.appendChild(linkText);
	entryName.appendChild(link);
	
	entryValue.innerHTML = 'Fingerprint: ' + pin.fingerprint;  
	entryValue.innerHTML += '<br>Pub Key Digest: ' + pin.subjPubKeyDigest;
	
	entryTR.appendChild(entryName);
	entryTR.appendChild(entryValue);
	entryTable.appendChild(entryTR);
	
}


function createHtmlTableSubjectEntry (fieldName, fieldValue, unpinUrl) {
	
	let entryTable = document.querySelector(".show-all-table");
	
	let entryTR = document.createElement("tr");
	let entryName =  document.createElement("td");
	let entryValue =  document.createElement("td");
	

	entryName.innerHTML = fieldName + '<br>';
	
	if (unpinUrl != false) {
		let link =  document.createElement("a");
		link.setAttribute("href", unpinUrl);
		let linkText = document.createTextNode("Remove this pin");
		link.appendChild(linkText);
		entryName.appendChild(link);
	}
	
	entryValue.innerHTML = fieldValue;
	
	entryTR.appendChild(entryName);
	entryTR.appendChild(entryValue);
	entryTable.appendChild(entryTR);
	
}


function checkIfDomainIsInUrlAndRemoveFromList() {
	
	var url_string = window.location.href
	var url = new URL(url_string);
	var domain = url.searchParams.get("domain");
	
	if (domain !=null) {
		bgPage.certDebugLog('cert-pin: Removing a pinned certificate for: ' + 
				            domain);

		bgPage.replaceOrRemoveAllPins(domain, false, "", false)
	}	
}

function checkIfFingerprintIsInUrlAndInvalidate() {
	
	var url_string = window.location.href
	var url = new URL(url_string);
	var fp = url.searchParams.get("fp");
	var domain = url.searchParams.get("dm");
	
	if (fp !=null) {
		bgPage.certDebugLog('cert-pin: Test-invaldating certificate for: ' + 
				            domain);

		// Set the validity date 5 minutes into the future so the record
		// will not immediately be deleted when the function to check
		// for expired certificates is run.
		var tempValidityDate = Date.now();
		tempValidityDate += 300 * 1000;
		
		bgPage.replaceOrRemoveAllPins(domain, "", "",
				                      tempValidityDate);
	}	
}

function getBaseUrlOfTab(){

	var urlWithParams = window.location.href;
	var tmpUrlArray = urlWithParams.split("?")
	return tmpUrlArray[0];
	
}



