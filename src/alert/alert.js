"use strict";

/**
* Certificate Pinner for Firefox
*
* Alert tab routines
*
* Contains the code for the alert tab that is opened by the background 
* routines when the fingerprint in the certificate given for a website
* does not match the stored fingerprint. The user then has the option to
* acknowledge the new certificate if he deems it to be genuine.
* 
* @version    1.0 2019-06-19
* @package    cert-pinner
* @copyright  Copyright (c) 2019 Martin Sauter
* @license    GNU General Public License
* @since      Since Release 1.0
* 
*/

// Local variables for main code and callback functions
var alertDetailsObject; // Contains page and cert details

// The following local variables contain a copy of particular information
// from the alertDetailsObject
var failedCert;
var domain;

// A variable to store information about this tab. Required e.g. in the
// onClose() handler
var tabInfo;

// Get the background page to access the offending certificate info and
// functions defined there.
const bgPage = browser.extension.getBackgroundPage();

// The tab id is required, e.g., to react to close tab events. Get it via
// a callback event.
var getTabInfo = browser.tabs.getCurrent();
getTabInfo.then(function(tabObj){
	bgPage.certDebugLog("Info for this tab");
	bgPage.certDebugLog(tabObj);
	tabInfo = tabObj;

	// Write tab id into to the record for this alert info tab. It can be used
	// later on to come back to the alert tab when the user tries to reload
	// the website for which an alert tab is already open. 
	// Note: Has to be in 'then' as the function requires tabInfo to be
	// populated which happens asynchronously.
	storeAlertTabIdToAlertInfoArray(bgPage);	
});

// If there is no information in the alert info array, there is something 
// wrong. Alert the user.
if (!bgPage.alertInfoArray) {
	alert('No Security Info Object received');
	exit;
}	

// Debug output of all entries of the alerted domains array
bgPage.certDebugLog('cert-pin: Array of currently alerted domains');
bgPage.certDebugLog(bgPage.alertInfoArray);

//Hide the optional Public Key pinning button if the option is not activated
if (bgPage.pubKeyPinningOn !== true) {
	showOrHideButtonAndText("pubKeyPinDiv", false);
}

// Get the domain name for which the certificate doesn't match from 
// from the request URL of the alert tab (inserted by background.js)
var url = new URL(document.URL);
var domain_id = decodeURI(url.searchParams.get("id"));
bgPage.certDebugLog(domain_id);
bgPage.certDebugLog("alert array:");
bgPage.certDebugLog(bgPage.alertInfoArray);

// Get the record from the alert info array for the domain
var alertDetailsObject = bgPage.alertInfoArray.find(function(record) {
	
	var domainInRecord = bgPage.getDomain(record.failedPageDetails.url);
	
	// Return true if the domain in the array matches the domain given in the
	// URL of the page.
	return domainInRecord == domain_id;
});

bgPage.certDebugLog('cert-pin: Found record: ');
bgPage.certDebugLog(alertDetailsObject);

failedCert = alertDetailsObject.failedPageSecurityInfo;
domain = bgPage.getDomain(alertDetailsObject.failedPageDetails.url);

createHtmlTableEntry('Domain', domain);
createHtmlTableEntry('Validity of Previous Cert.', getValidityofPrevCert(domain));
createHtmlTableEntry('New Cert Details:', '');

// Output details for each of the chained certificates
failedCert.certificates.forEach(cert => {
	outputCertificateDetails(cert);
});

// Hide the 'add fingerprint text and button' if pinned on the key.
// Otherwise the user might click on add fingerprint in which case
// there would be a pin on the fingerprint and a pin on the public key
// which is not allowed as this doesn't make sense.
if (isDomainPinnedOnPubKey(domain)) {
	showOrHideButtonAndText("addFingerprintDiv", false);	
}

// And finally, put button click handler function in place for updating 
// the certificate
document.getElementById("acceptBtn").onclick = acceptNewPin;
document.getElementById("acceptNewPubKeyBtn").onclick = acceptNewPin;
document.getElementById("addBtn").onclick = acceptAdditionalFingerprint;



// Run a cleanup function when the alert tab is closed
browser.tabs.onRemoved.addListener(tabCloseHandler)

/*****************************************************************************
 * 
 * Event Handlers and support functions
 * 
 */

/**
 * 
 * storeAlertTabIdToAlertInfoArray()
 * 
 * @param object bgPage reference to the background script
 * 
 * @return none
 * 
 */

function storeAlertTabIdToAlertInfoArray(bgPage) {
	var i = bgPage.alertInfoArray.findIndex(function(record) {

		var domainInRecord = bgPage.getDomain(record.failedPageDetails.url);
		
		// Return true if the domain in the array matches the domain given in the
		// URL of the page.
		return domainInRecord == domain_id;	
	});

	bgPage.certDebugLog('cert-pin: Index of alert record: ' + i);
	bgPage.certDebugLog(alertDetailsObject);

	// now store this tab's id to the AlertInforArray
	bgPage.alertInfoArray[i].AlertTabId = tabInfo.id;
	
	bgPage.certDebugLog('cert-pin: Alert Tab ID is: ' 
			             + bgPage.alertInfoArray[i].AlertTabId);
	
}


function tabCloseHandler(tabId, removeInfo) {
	
	// Exit if the handler is not called for this alert tab
	if (tabInfo.id != tabId) return;
	
	bgPage.certDebugLog('cert-pin: Alert tab closes');
		
	// When the alert page closes, remove the entry from the 
	// alert details array in background.js	
	bgPage.removeDomainFromAlertList(domain);
}

function outputCertificateDetails (certificate){
	
	createHtmlTableEntry('Subject', certificate.subject);	
	createHtmlTableEntry('Fingerprint', certificate.fingerprint.sha256);
	createHtmlTableEntry('Public Key', certificate.subjectPublicKeyInfoDigest.sha256);
	createHtmlTableEntry('Issuer', certificate.issuer);
	createHtmlTableEntry('Serial Num', certificate.serialNumber);
	
	var d = new Date(certificate.validity.start);
	var n = d.toLocaleDateString(bgPage.localDateTimeFormat) + ', ' + 
	        d.toLocaleTimeString(bgPage.localDateTimeFormat);
	createHtmlTableEntry('Start Validity', n);
	
	var d = new Date(certificate.validity.end);
	var n = d.toLocaleDateString(bgPage.localDateTimeFormat) + ', ' + 
	        d.toLocaleTimeString(bgPage.localDateTimeFormat);	
	createHtmlTableEntry('End Validity', n);
	
}

function createHtmlTableEntry (fieldName, fieldValue) {
	
	let entryTable = document.querySelector(".mismatch-table");
	entryTable.classList.remove("hidden");
	
	let entryTR = document.createElement("tr");
	let entryName =  document.createElement("td");
	let entryValue =  document.createElement("td");
	
	entryName.textContent = fieldName;
	entryValue.textContent = fieldValue;
	
	entryTR.appendChild(entryName);
	entryTR.appendChild(entryValue);
	entryTable.appendChild(entryTR);
	
}


function getValidityofPrevCert(domain) {

	var details = bgPage.getPinnedDomainInfo(domain);

	bgPage.certDebugLog('cert-pin: domain details');
	bgPage.certDebugLog(details);
	
	var d = new Date(details[0].validity);
	var n = d.toLocaleDateString(bgPage.localDateTimeFormat) + ', ' + 
	        d.toLocaleTimeString(bgPage.localDateTimeFormat);	
	
	return(n);
}


function isDomainPinnedOnPubKey(domain) {
	
	var details = bgPage.getPinnedDomainInfo(domain);
	
	if (details[0].subjPubKeyDigest != "") {
		
		bgPage.certDebugLog('cert-pin: domain is pinned on public key');
		bgPage.certDebugLog(details[0]);
		bgPage.certDebugLog(details[0].subjPubKeyDigest);
		return true;
	}
	
	bgPage.certDebugLog('cert-pin: domain is pinned on fingerprint');
	return false;	
}

/**
 * 
 * acceptNewPin()
 * 
 * Accepting a new fingerprint/pub key for pinning consists of:
 * 
 *  * Updating the data structures and writing them to storage
 *  * Closing the alert tab
 *  * Reloading the original tab
 *  
 *  Close and reload are asynchronous operations so handlers and
 *  callback functions are required that follow after this function.
 * 
 * @param none
 * 
 * @return none
 * 
 */

function acceptNewPin() {
	
	var fingerprint = failedCert.certificates[0].fingerprint.sha256;
	var subjPubKeyDigest = failedCert.certificates[0].subjectPublicKeyInfoDigest.sha256;
	var validity = failedCert.certificates[0].validity.end;
	
	// Select either the fingerprint or the pub key for pinning.
	if (this.id == "acceptBtn") {
		subjPubKeyDigest = "";
	} else {
		fingerprint = "";
	}	
	
	bgPage.certDebugLog('cert-pin: acceptNewPin - ' + domain + ' - ' + 
			            fingerprint);
	
	if (!bgPage.replaceOrRemoveAllPins(domain, fingerprint, subjPubKeyDigest, validity)) {
		alert('ERROR: Unable to update pinning record!?');
	}
	
	// Get the object representing the alert tab so we can close it and
	// forward it to a callback function that does the actual
	// close and reload of the original tab that caused the alert.
	// Done asynchronously so not part of this function.
	closeThisTabAndReloadOriginalTab(callbackFunctionCloseReload);
}


/**
 * 
 * acceptAdditionalFingerprint()
 * 
 * Accepting an additional fingerprint consists of:
 * 
 *  * Updating the data structures and writing them to storage
 *  * Closing the alert tab
 *  * Reloading the original tab
 *  
 *  Close and reload are asynchronous operations so handlers and
 *  callback functions are required that follow after this function.
 * 
 * @param none
 * 
 * @return none
 * 
 */

function acceptAdditionalFingerprint() {
	
	var fingerprint = failedCert.certificates[0].fingerprint.sha256;
	var validity = failedCert.certificates[0].validity.end;
	
	bgPage.certDebugLog('cert-pin: acceptAdditionalFingerprint - ' + domain + ' - ' + 
			            fingerprint);
	
	if (!bgPage.addAdditionalPin(domain, fingerprint, validity)) {
		alert('ERROR: Unable to add fingerprint!?');
	}
	
	// Get the object representing the alert tab so we can close it and
	// forward it to a callback function that does the actual
	// close and reload of the original tab that caused the alert.
	// Done asynchronously so not part of this function.
	closeThisTabAndReloadOriginalTab(callbackFunctionCloseReload);
}


function closeThisTabAndReloadOriginalTab(callback) {
  getCurrentWindowTabs().then((tabs) => {
    for (var tab of tabs) {
      if (tab.active) {
        callback(tab);
      }
    }
  });
}

function getCurrentWindowTabs() {
	  return browser.tabs.query({currentWindow: true});
}

function callbackFunctionCloseReload(tab) {
	
	// Switch to the tab that produced the alert and reload
	bgPage.certDebugLog('cert-pin: Closing alert, switch and reload original tab');
	
    browser.tabs.update(alertDetailsObject.failedPageDetails.tabId, {
        active: true,
        url: alertDetailsObject.failedPageDetails.url
    });

	// When the alert page closes, remove the entry from the 
	// alert details array in background.js	
	bgPage.removeDomainFromAlertList(domain);
	
	// And finally, remove this 'alert' tab and we are done!
	browser.tabs.remove(tab.id);	
}

function showOrHideButtonAndText(divId, show) {
	var x = document.getElementById(divId);
	
	if (show === true) {
		x.style.display = "block";
	} else {
		x.style.display = "none";
	}
}




